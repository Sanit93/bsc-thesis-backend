package com.example.inzynierka.controller;

import com.example.inzynierka.model.User;
import com.example.inzynierka.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequiredArgsConstructor
public class HelloController {

    private final UserService userService;

    @GetMapping
    public Mono<Object> index(@AuthenticationPrincipal Mono<Object> user) {
//        System.out.println(oauth2User.block().toString());

        return user;
    }

    @PostMapping("register")
    public Mono<User> register(String email, String password) {
        return userService.save(email, password);
    }

    @GetMapping("all")
    public String get() {
        return "Everyone have access to this";
    }

    @GetMapping("logged")
    public String getForLogged() {
        return "Only authorized have access";
    }
}
