package com.example.inzynierka.service;

import com.example.inzynierka.model.User;
import com.example.inzynierka.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
@RequiredArgsConstructor
@PreAuthorize("hasAnyRole('ADMIN', 'USER')")
public class UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    public Mono<User> save(String email) {
        return userRepository.save(new User(email));
    }

    public Mono<User> save(String email, String password) {
        System.out.println(email);
        System.out.println(password);
        return userRepository.save(new User(email,passwordEncoder.encode(password)));
    }

    @PreAuthorize("hasRole['ADMIN']")
    public Mono<UserDetails> get(String email) {
        return userRepository.findByUsernameIgnoreCase(email);
    }
}
