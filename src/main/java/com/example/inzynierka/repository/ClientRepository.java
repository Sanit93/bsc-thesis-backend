package com.example.inzynierka.repository;

import com.example.inzynierka.model.User;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.web.server.ServerOAuth2AuthorizedClientRepository;
import org.springframework.security.oauth2.core.user.DefaultOAuth2User;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

@Slf4j
@Component
@RequiredArgsConstructor
public class ClientRepository implements ServerOAuth2AuthorizedClientRepository {

    private final UserRepository userRepository;

    @Override
    public <T extends OAuth2AuthorizedClient> Mono<T> loadAuthorizedClient(String clientRegistrationId, Authentication principal, ServerWebExchange exchange) {
        log.info("LOAD AUTHORIZED CLIENT");
        return null;
    }

    @Override
    public Mono<Void> saveAuthorizedClient(OAuth2AuthorizedClient authorizedClient, Authentication principal, ServerWebExchange exchange) {
        DefaultOAuth2User oAuth2User = (DefaultOAuth2User) principal.getPrincipal();

        User user = new User(oAuth2User.getAttribute("email"));
        return userRepository.save(user)
                .onErrorResume(throwable -> Mono.empty())
                .then();
    }

    @Override
    public Mono<Void> removeAuthorizedClient(String clientRegistrationId, Authentication principal, ServerWebExchange exchange) {
        log.info("REMOVE AUTHORIZED CLIENT");
        return null;
    }
}
