package com.example.inzynierka.repository;

import com.example.inzynierka.model.User;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;
import org.springframework.security.core.userdetails.UserDetails;
import reactor.core.publisher.Mono;

@EnableReactiveMongoRepositories
public interface UserRepository extends ReactiveMongoRepository<User, String> {

    Mono<UserDetails> findByUsernameIgnoreCase(String email);

    Mono<Boolean> existsByUsername(String username);
}
